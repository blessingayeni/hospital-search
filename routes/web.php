<?php

//Route::get('/', function () {
    //return view('pages.home');
//});


Route::get('/', function () {
    return view('pages.signin');
});


Route::get('search', 'HospitalController@viewSearch')->middleware('jwt.auth')->name('blood.search');

Route::get('/search-result', function () {
    return view('pages.search_result');
})->middleware('auth');

Route::group(['prefix' => 'api/v1'], function(){

	Route::post('signup' , 'HospitalController@store');

	Route::post('login', 'HospitalController@authenticate');
	
	Route::post('search', [ 'as' => 'search', 'uses' => 'HospitalController@search'])->middleware('jwt.auth');

});