<div class="sign_in">

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	<form method="POST" action="{{URL::to('api/v1/login')}}">

		<input type="text" name="username" id="username" placeholder="Username" value="{{ old('username') }}">

		<input type="password" name="password" id="password"  placeholder="Password" value="{{ old('password') }}">

		{{ csrf_field() }}

		<button>Sign In</button>

		</form>

		<a href="{{ URL::to('/') }}" class="request_acc">request for an account</a>

</div>