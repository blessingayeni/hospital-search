<h3>create your account</h3>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="{{ URL::to('api/v1/signup') }}">

    <input type="text" name="hospital_name" id="hospital_name" placeholder="Name of Hospital" value="{{ old('hospital_name') }}">
   
    <input type="text" name="address" id="address" placeholder="Address" value="{{ old('address') }}">
    

    <input type="text" name="state" id="state"  placeholder="State" value="{{ old('state') }}">
   
    <input type="text" name="country" id="country"  placeholder="Country" value="{{ old('country') }}">
    
 
    <input type="text" name="contact_person" id="contact_person" placeholder="Name of Contact Person" value="{{ old('contact') }}">
    
             
    <input type="text" name="phone" id="phone" placeholder="Phone Number" value="{{ old('phone') }}">
    
    <input type="text" name="username" id="username" placeholder="Username" value="{{ old('username') }}">
    

    <input type="password" name="password" id="password"  placeholder="Password" value="{{ old('password') }}">
    

    <input type="password" name="retype_password" id="retype_password"  placeholder="Retype Password"  value="{{ old('retype_password') }}">
   
    {{ csrf_field() }}

    <button class="signup">Sign Up</button>

</form>


<a href="{{ URL::to('/sign-in') }}" class="signin">sign in to your account</a>
