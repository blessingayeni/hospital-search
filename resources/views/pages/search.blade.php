@extends('layout2')

	@section('title')

		Search

	@stop


	@section('sidebar')
	  
		{{--@include('includes.welcome')--}}
		<div class="side2">
			<h3>Welcome</h3>
			
			<h1 class="hospital_name">{{ $user->hospital_name }}</h1>
	
			<div class="contact">

			<p>&copy; Redbank. All rights reserved.</p>
			
			</div>
		</div>
	    
	@stop


	@section('content')

		{{--@include('includes.search_form')--}}

		<span class="head">Ready When you are&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" action="{{ URL::to('api/v1/search') }}?token={{$_GET['token']}}">

			<h3>Your search location</h3>
			<input type="hidden" name="user_id" value="{{$user->id}}">

	    	<select name="state" id="state">
	    		
	    		<option value="">State</option>
	    		@foreach($states as $state)

	    			<option value="{{ $state-> id}}">{{ $state-> name}}</option>

	    		@endforeach
	    	</select>
	    
	    	<select name="lga" id="lga">
	    		
	    		<option value="">LGA</option>
	    		@foreach($lgas as $lga)

	    			<option value="{{ $lga-> id}}">{{ $lga-> name}}</option>

	    		@endforeach
	    		
	    	</select>
	          

			<h3>Blood details</h3>

			<select name="blood_type" id="type">
	    		
	    		<option value="">Blood Type</option>
	    		<option value="total_o_positive">O Positive</option>
	    		<option value="total_o_negative">O Negative</option>
	    		<option value="total_a_positive">A Positive</option>
	    		<option value="total_a_negative">A Negative</option>
	    		<option value="total_b_positive">B Positive</option>
	    		<option value="total_b_negative">B Negative</option>
	    		<option value="total_ab_positive">AB Positive</option>
	    		<option value="total_ab_negative">AB Negative</option>

	    	</select>
	    
	    	{{ csrf_field() }}

	    	 <input type="text" name="pints" id="pints"  placeholder="No of pints"  value="{{ old('pints') }}">
	    	 <br>

	    	<button class="search">Search</button>
		</form>


	@stop
