@extends('layout2')

	@section('title')

		Search Result

	@stop


	@section('sidebar')
	  
		{{--@include('includes.welcome')--}}

		<h3>Welcome</h3>
		<h1 class="hospital_name">{{ $user->hospital_name }}</h1>

		<h1 class="search_menu">SEARCH</h1>

		<div class="contact">

		<p>&copy; Redbank. All rights reserved.</p>
		
		</div>
		
	    
	@stop


	@section('content')

		{{--@include('includes.search_form')--}}

		<span class="head">Your search result for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<br><br>
		
			{{--<div class="result_input">
			
				<div class="row">

					<div class="col-md-3">

						<p>Blood Type</p>

					</div>

					<div class="col-md-3">

						<p>LGA</p>
						
					</div>

					<div class="col-md-3">

						<p>State</p>
						
						
					</div>

					<div class="col-md-3">

						<p>Pint</p>
						
					</div>

				</div>
			</div>--}}
		 <br><br>
	    	<p>

	    		<em><b>{{ count($query1) }} 
	    		@if(count($query1) == 1)

	    			{{ "Result"}}
	    		
	    		@else

	    			{{ "Results" }}
	    		
	    		@endif
	    	</b></em></p>
	    	@if(count($query1) == 0)
			

				<p class="no_result">
					Kindly Contact <b>Redbank</b><br>
					09090005267
				</p>
	    	@else
			
				@foreach($query1 as $query)
					
					<div class="results">

			    		<div class="row">
			    			
				    		<div class="col-md-8">

				    			<h3>{{ $query->name }}</h3>

				    			<p class="address">{{ $query->address }}</p>

					    		<div class="row">

					    			<div class="col-md-4">

					    			<span class="title">CALL</span><br>
					    					
					    				
					    				<span class="info">{{ $query->primary_phone }}</span>
					    				
					    			</div>

					    			<div class="col-md-4">

					    					<span class="title">PRICE</span><br>
					    				
					    				<span class="info">Negotiatiable</span>
					    			</div>

					    			<div class="col-md-4">
					    			
					    				<span class="title">DELIVERY</span><br>
					    				
					    				<span class="info">YES</span>

					    			</div>
					    		</div>
					    		<br>
					    		{{--<span class="title">HIDE BAG NUMBERS</span><br>
					    				
					    		<span class="info">A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352<br>
					    		A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352&nbsp;&nbsp;A0352
					    		</span>--}}
				    		</div>

				    	<div class="col-md-2 item">

				    		<span class="badge notify-badge">{{ $query->$val}}</span>
				    		<img src="{{ URL::to($img)}}" width="100px">

				    	</div>

			    	</div>
			    </div>
			    <br>
				@endforeach
			
			@endif

			<a href="{{URL::to('/search')}}?token={{$_GET['token']}}" class="search">Search Again</a>

	@stop