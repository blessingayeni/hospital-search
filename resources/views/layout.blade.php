<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Hospital Search :: @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ URL::to('styler.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/app.css') }}">
	<style type="text/css">@font-face { font-family:Oswald; src: url('{{ asset('font/Oswald-Light.ttf') }}'); }</style>
</head>
<body>

<div class="container-fluid">
	<div class="row wrapper">

		<div class="col-md-8 content">
		
			@yield('content')
		</div>
		
		<div class="col-md-4 side">
			@yield('sidebar')
		</div>
	           
		@yield('footer')
	</div>	
</div>	

</body>

<script type="text/javascript" src="{{ URL::to('/css/app.js') }}"></script>
</html>

