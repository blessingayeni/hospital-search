<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloodbank extends Model
{
    //

    protected $table = 'blood_banks';

    public function lga()
    {
    	$this->belongsTo(Lga::class);
    }

    public function state()
    {
    	$this->belongsTo(State::class);
    }

}
