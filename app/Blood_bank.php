<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blood_bank extends Model
{
    //
    protected $table = 'blood_banks';

    public function state()
    {
        return $this->hasOne('App\State','id','state_id');
    }
    public function lga()
    {
        return $this->hasOne('App\Lga','id','lga_id');
    }
}
