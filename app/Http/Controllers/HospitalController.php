<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Socialite;

use App\User;

use App\State;

use App\Lga;

use App\Search;

use App\Blood_bank;

use App\Mail\UserRegistered;

use Illuminate\Support\Facades\Mail;

use Validator;

use Tymon\JWTAuth\Facades\JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;

use App\Transformer\UserTransformer;

use App\Transformer\BloodbankTransformer;

class HospitalController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'hospital_name' => 'required',
            'address' => 'required',
            'state' => 'required',
            'country' => 'required',
            'contact_person' => 'required',
            'phone' => 'required',
            'username' => 'required',
            'password' => 'required',
            'retype_password' => 'required'

        ]);

        if($validator->fails())
        {
            //return $this->errorUnauthorized('Ensure you fill all field don\'t leave any empty');
             return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }


        $newUser =  User::create([

            'hospital_name' => $request->hospital_name,
            'address' => $request->address,
            'state' => $request->state,
            'country' => $request->country,
            'contact_person' => $request->contact_person,
            'phone' => $request->phone,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'retype_pass' => bcrypt($request->password),


        ]);
         if($newUser)
         {
            Mail::to('ayeniblessing32@gmail.com')->send(new UserRegistered);
         }
            

        //return $this->responseWithJson('User Registration successful');
         return redirect('/sign-in');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);

        if(! $user)
        {
            return $this->errorNotFound('Not Found');
        }

        return $this->respondWithItem($user, new UserTransformer);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Authenticate a User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function authenticate(Request $request)
    {

         $this->validate($request, [
        'username' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

            try
            {
                if(! $token = JWTAuth::attempt($credentials))
                {
                    //return response()->json(['error' => 'invalid_credentials'], 401);
                    return redirect('/sign-in')
                                    ->withErrors(['Invalid Username or Password']);

                }

            }
            catch(JWTException $e)
            {
                //return response()->json(['error' => 'could_not_create_token'], 500);
                 return redirect('/sign-in')
                                    ->withErrors(['Invalid User']);
            }
            $token  = array('token' => $token );

            return redirect()->route('blood.search',$token);  
      }

    public function viewSearch(Request $request)
    {

        $states = State::all();
        $lgas = Lga::all();
        $user = JWTAuth::toUser($_GET['token']);

        return view('pages.search', compact(['states', 'lgas', 'user']));
    }

    public function search(Request $request)
    {
         $this->validate($request, [
            'state' => 'required',
            'lga' => 'required',
            'blood_type' => 'required',
            'pints' => 'required',
        ]);

        //$query1 = Blood_bank::where($request->blood_type,'>=',(int) $request->pints)
            $query1 = Blood_bank::where($request->blood_type,'>',0)
           ->where('state_id' , $request->state)
           ->where('lga_id' , $request->lga)
           ->get();

        if($query1)
        {
           $newSearch = Search::create([
             'state_id' => $request->state,
             'lga_id'  => $request->lga,
             'blood_type' => $request->blood_type,
             'no_of_pints' => $request->pints,
             'user_id' => $request->user_id,

            ]);

           if($newSearch)
           {
            $data = array('blood' => $newSearch->blood_type );
            //dd($newSearch->id);
            Mail::send('emails.search', $data, function($message){
                $message->to('ayeniblessing32@yahoo.com')->subject('New Search');

            });
           }

      }

       $img= '/images/'.$request->blood_type.'.png';
       $val = $request->blood_type;
       $user = JWTAuth::toUser($_GET['token']);
       return view('pages.search_result', compact(['query1','img','val','user']));
    }

   
    
}
