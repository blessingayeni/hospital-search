<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    //
    protected $fillable = ['user_id', 'state_id', 'lga_id', 'blood_type', 'no_of_pints'];

    public function state()
    {
        return $this->hasMany('App\State','id','state_id');
    }
    public function lga()
    {
        return $this->hasMany('App\Lga','id','lga_id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }


}
