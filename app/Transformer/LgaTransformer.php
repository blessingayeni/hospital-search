<?php namespace App\Transformer;

use App\Lga;

use League\Fractal\TransformerAbstract;

class LgaTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Lga $lga)
    {
        return [
        
            'lga_name'  => $lga->name,
            
        ];
    }
}