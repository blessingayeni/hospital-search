<?php namespace App\Transformer;

use App\State;

use League\Fractal\TransformerAbstract;

class StateTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(State $state)
    {
        return [
        
            'state_name'  => $state->name,
            
        ];
    }
}