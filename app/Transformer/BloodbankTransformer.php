<?php namespace App\Transformer;

use App\Bloodbank;
use League\Fractal\TransformerAbstract;

class BloodbankTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to embed via this processor
     *
     * @var array
     */
    protected $availableEmbeds = [
        'state',
        'lga',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Bloodbank $bloodbank)
    {
        return [
            'id'          => (int) $bloodbank->id,
            'name'  => (string) $bloodbank->name,
            

        ];
    }

    /**
     * Embed Place
     *
     * @return League\Fractal\Resource\Item
     */
    public function embedState(Bloodbank $bloodbank)
    {
        $state = $bloodbank->state_id;

        return $this->item($state, new StateTransformer);
    }

    /**
     * Embed User
     *
     * @return League\Fractal\Resource\Item
     */
    public function embedLga(Bloodbank $blood_bank)
    {
        $lga = $bloodbank->lga;

        return $this->item($lga, new LgaTransformer);
    }
}
