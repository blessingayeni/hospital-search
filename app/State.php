<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
 
   //
	protected $table = 'states';

    public function blood_banks()
    {
    	$this->hasMany(Blood_bank::class);
    }

    public function searches()
    {
    	$this->hasMany(Search::class);
    }
}
